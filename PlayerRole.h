#ifndef PLAYERROLE_H
#define PLAYERROLE_H
#include "Player.h"

class PlayerRole:public  Player
{
protected:
    Player  *player;


public:
   void PassBall()
   {
        player->PassBall();
   }


    void AssignPlayer(Player *p)
    {
        player = p;
    }

    PlayerRole():Player()
    {

    }
};

#endif // PLAYERROLE_H
