#ifndef TEAM_H
#define TEAM_H
#include "TeamStrategy.h"
#include <iostream>
#include <string>
using namespace  std;

class Team
{
private:
    string teamName;
    TeamStrategy *strategy;


public:
    void SetStrategy(TeamStrategy* s)
    {
        strategy = s;
    }



   void playGame()
   {
       cout << teamName <<endl;
       strategy->play();
    }


   Team(string teamName)
   {
    this->teamName = teamName;
   }
};


#endif // TEAM_H
