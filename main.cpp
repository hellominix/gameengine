#include <QCoreApplication>
#include "Ball.h"
#include "Basketball.h"
#include "Observer.h"
#include "Player.h"
#include "Position.h"
#include "Reference.h"
#include "Team.h"
#include "TeamStrategy.h"
#include "Attackstrategy.h"
#include "Defendstrategy.h"
#include "PlayerRole.h"
#include "Defender.h"
#include "FieldPlayer.h"
#include "MidFielder.h"
#include "GoalKeeper.h"
#include "Forward.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Basketball *ball = new Basketball();

    Player *Owen = new Player(ball, "Owen");
    Player *Ronaldo = new Player(ball, "Ronaldo");
    Player *Rivaldo = new Player(ball, "Rivaldo");

    Referee *Mike = new Referee(ball, "Mike");
    Referee *John = new Referee(ball, "John");

    ball->AttachObserver(Owen);
    ball->AttachObserver(Ronaldo);
    ball->AttachObserver(Rivaldo);
    ball->AttachObserver(Mike);
    ball->AttachObserver(John);

    cout<<"After attaching the observers..."<<endl;

    ball->SetBallPosition(new Position(0,0,0));

    ball->DetachObserver(Owen);
    ball->DetachObserver(John);


    cout<<"After detaching Owen and John..."<<endl;

    ball->SetBallPosition(new Position(10, 10, 30));



    AttackStrategy *attack = new AttackStrategy();
    DefendStrategy *defend = new DefendStrategy();

    Team *france = new Team("France");
    Team *italy = new Team("Italy");

    cout<<"Setting the strategies.."<<endl;


    france->SetStrategy(attack);
    italy->SetStrategy(defend);


    france->playGame();
    italy->playGame();


    cout<<"Changing the strategies.."<<endl;


    france->SetStrategy(defend);
    italy->SetStrategy(attack);



    france->playGame();
    italy->playGame();



    FieldPlayer *owen = new FieldPlayer("Owen");
    FieldPlayer *beck =new FieldPlayer("Beckham");
    GoalKeeper *khan = new GoalKeeper("Khan");


    cout << " > Warm up Session... "<<endl;

    owen->PassBall();
    beck->PassBall();
    khan->PassBall();

    cout << " > Match is starting.. " <<endl;

     Forward *forward1 = new Forward();
     forward1->AssignPlayer(owen);

     MidFielder *midfielder1 = new MidFielder();
     midfielder1->AssignPlayer(beck);

     forward1->PassBall();
     forward1->ShootGoal();

     midfielder1->PassBall();
     midfielder1->Dribble();

     cout<<" > OOps, Owen got injured. Jerrard replaced Owen.. " << endl;


     FieldPlayer *jerrard = new FieldPlayer("Jerrard");


     forward1->AssignPlayer(jerrard);
     forward1->ShootGoal();

     Forward *onemoreForward = new Forward();
     onemoreForward->AssignPlayer(beck);

     cout<<" > Beckham has multiple responsibilities.. "<<endl;

     onemoreForward->ShootGoal();
     midfielder1->Dribble();

    return a.exec();
}
