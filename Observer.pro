#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T20:06:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Observer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    Ball.h \
    Observer.h \
    Position.h \
    Basketball.h \
    Player.h \
    Reference.h \
    Attackstrategy.h \
    Defendstrategy.h \
    Team.h \
    TeamStrategy.h \
    FieldPlayer.h \
    GoalKeeper.h \
    PlayerRole.h \
    Forward.h \
    MidFielder.h \
    Defender.h
