#ifndef DEFENDSTRATEGY_H
#define DEFENDSTRATEGY_H
#include "TeamStrategy.h"
#include <iostream>
using namespace std;

class DefendStrategy:public TeamStrategy
{
public:
    void play()
    {
        cout<<" Playing in defensive mode" << endl;
    }
};


#endif // DEFENDSTRATEGY_H
