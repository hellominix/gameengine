#ifndef DEFENDER_H
#define DEFENDER_H
#include <iostream>
#include "PlayerRole.h"
using namespace std;

class Defender:public PlayerRole
{
public:
    void Defend(){

       cout<<" Defender "<< this->GetName()<<" defended the ball" <<endl;
    }

};


#endif // DEFENDER_H
