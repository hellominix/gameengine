#ifndef GOALKEEPER_H
#define GOALKEEPER_H
#include <iostream>
#include "Player.h"
#include <string>

using namespace std;

class GoalKeeper:public Player
{

public:
    void PassBall ()
    {
        cout << " GoalKeeper "<<this->GetName() <<" passed the ball" <<endl;
    }


    GoalKeeper(string playerName ):Player(playerName)
    {

    }
};


#endif // GOALKEEPER_H
