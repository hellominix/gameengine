#ifndef REFERENCE_H
#define REFERENCE_H

#include <stdio.h>
#include <string>
#include "Position.h"
#include "Basketball.h"
#include "Observer.h"
using namespace std;

class Referee:public Observer
{
private:
    Position ballPosition;
    Basketball *ball;
    string myName;


public:
    void Update()
    {
        ballPosition = ball->GetBallPosition();
        cout <<"Referee " << myName <<" say that the ball is at  " << ballPosition.X <<"   "<<ballPosition.Y<<"   "<<ballPosition.Z<<endl;
    }


    Referee(Basketball*b, string refereeName)
    {
        myName = refereeName;
        ball = b;
    }

};


#endif // REFERENCE_H
