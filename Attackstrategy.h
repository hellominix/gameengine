#ifndef ATTACKSTRATEGY_H
#define ATTACKSTRATEGY_H
#include "TeamStrategy.h"
#include <iostream>
using namespace std;

class AttackStrategy:public TeamStrategy
{
  public:
    void play()
    {
        cout<<" Playing in attacking mode"<<endl;
    }
};


#endif // ATTACKSTRATEGY_H
