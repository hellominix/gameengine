#ifndef POSITION_H
#define POSITION_H

class Position
{
public:
    int X;
    int  Y;
    int  Z;

    Position(int x = 0, int y = 0, int z = 0)
    {
        X = x;
        Y = y;
        Z = z;
    }
};



#endif // POSITION_H
