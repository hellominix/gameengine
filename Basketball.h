#ifndef BASKETBALL_H
#define BASKETBALL_H
#include "Ball.h"
#include "Position.h"

class Basketball: public Ball
{
private:
   Position *myPosition;

public:
   Position GetBallPosition()
   {
    return *myPosition;
   }


   void SetBallPosition(Position *p)
   {
      myPosition = p;
      NotifyObservers();
   }
};


#endif // BASKETBALL_H
