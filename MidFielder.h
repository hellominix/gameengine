#ifndef MIDFIELDER_H
#define MIDFIELDER_H
#include <iostream>
#include "PlayerRole.h"
using namespace std;

class MidFielder:public PlayerRole
{
public:
    void Dribble()
    {
        cout <<" Midfielder "<< player->GetName()<<" dribbled the ball" <<endl;
   }

};


#endif // MIDFIELDER_H
