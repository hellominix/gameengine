#ifndef TEAMSTRATEGY_H
#define TEAMSTRATEGY_H

class TeamStrategy
{
public:
    virtual void play()=0;
};

#endif // TEAMSTRATEGY_H
