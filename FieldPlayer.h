#ifndef FIELDPLAYER_H
#define FIELDPLAYER_H
#include <iostream>
#include <string>
#include "Player.h"
using namespace std;

class FieldPlayer:public Player
{

public:
    void PassBall ()
    {
        cout<<" Fieldplayer " << this->GetName()<<" passed the ball"  <<endl;
    }


    FieldPlayer(string playerName):Player(playerName)
    {

    }
};


#endif // FIELDPLAYER_H
