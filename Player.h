#ifndef PLAYER_H
#define PLAYER_H
#include "Position.h"
#include "Observer.h"
#include "Basketball.h"
#include <iostream>
#include <string>
using namespace std;

class Player:public Observer
{
private:
    Position ballPosition;
    string myName;
    Basketball *ball;

public:
    void Update ()
    {
        ballPosition = ball->GetBallPosition();
        cout <<"Player " << myName << " say that the ball is at " << ballPosition.X <<"  "<< ballPosition.Y<<"  " << ballPosition.Z << endl;
    }

    string GetName()
    {
        return myName;
    }

    void SetName(string value)
    {
        myName = value;
    }


    virtual void PassBall()
    {

    }

    Player(Basketball *b,string playerName)
    {
        ball = b;
        myName = playerName;
    }

    Player(string playerName)
    {
        myName = playerName;
    }

    Player()
    {

    }
};


#endif // PLAYER_H
