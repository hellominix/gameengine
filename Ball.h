#ifndef BALL_H
#define BALL_H
#include <list>
#include "Observer.h"
using namespace std;

class Ball
{
private:
  list <Observer *> observersList;

public:
 void AttachObserver(Observer* obj )
 {
    observersList.push_back(obj);
 }

void DetachObserver(Observer* obj)
{
    observersList.remove(obj);
}


void NotifyObservers()
{
        foreach (Observer *o, observersList)
        {
            o->Update();
        }
}

};
#endif // BALL_H
